require 'test_helper'

class UserTest < ActiveSupport::TestCase
    def setup
        @user = User.new(name: "Example User", email: "user@example.com", password: "foobar", password_confirmation: "foobar")
    end

    test "should be valid" do
        assert @user.valid?
    end

    test "name should be present" do
        @user.name = ""
        assert_not @user.valid?
    end

    test "email should be present" do
        @user.email = ""
        assert_not @user.valid?
    end

    test "length of name should not be exceed 50" do
        @user.name = "a" * 51
        assert_not @user.valid?
    end

    test "length of email should not be exceed 255" do
        @user.email = "a" * 256
        assert_not @user.valid?
    end

    test "email format should be valid" do
        valid_email_address = %w(user@example.com USER@foo.COM A_US-ER@foo.bar.org first.last@foo.jp alice+bob@baz.cn)
        valid_email_address.each do |email|
            @user.email = email
            assert @user.valid?, "#{email.inspect} should be valid."
        end
    end

    test "email validation should reject invalid email address" do
        invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com foo@bar..com ]
        invalid_addresses.each do |email|
            @user.email = email
            assert_not @user.valid?, "#{email.inspect} should be invalid."
        end
    end

    test "email address should be unique" do
        duplicated_user = @user.dup
        @user.save
        assert_not duplicated_user.valid?
    end

    test "email address should be case insensitive" do
        duplicated_user = @user.dup
        @user.save
        duplicated_user.email = @user.email.upcase
        assert_not duplicated_user.valid?
    end

    test "password should be present" do
        @user.password = @user.password_confirmation = " " * 6
        assert_not @user.valid?
    end

    test "password should have a minimum length of 5" do
        @user.password = @user.password_confirmation = 'a' * 5
        assert_not @user.valid?
    end
end
